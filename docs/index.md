
# Test Page

## Collapsible Blocks

??? note "Block closed by default"

    The text is inside a collapsible block.

???+ note "Block open by default:"

    The text $\sqrt 2$ is inside a collapsible block.
# Insertion de graphiques en langages toto

Module de Rodrigo Schwencke, voir  <https://pypi.org/project/mkdocs-markdown-graphviz/> et <https://gitlab.com/rodrigo.schwencke/mkdocs-markdown-graphviz>.

## TEST Syntaxe toto

```dot
digraph G {
    rankdir=LR
    TerreTest [peripheries=2]
    Mars
    TerreTest -> Mars
}
```

???+ note "Open Block with toto" 

    ```dot
    digraph G {
        rankdir=LR
        TerreTest [peripheries=2]
        Mars
        TerreTest -> Mars
    }
    ```

???- note "Collapsed Block with toto" 

    ```dot
    digraph G {
        rankdir=LR
        TerreTest [peripheries=2]
        Mars
        TerreTest -> Mars
    }
    ```

```dot
digraph G {
    rankdir=LR
    TerreTest [peripheries=2]
    Mars
    TerreTest -> Mars
}
```

## EST Syntaxe graphviz toto filename.svg

```graphviz dot un.svg
digraph G {
    rankdir=LR
    TerreTest [peripheries=2]
    Mars
    TerreTest -> Mars
}
```

???+ note "Open Block with graphviz"

    ```graphviz dot deux.svg
    digraph G {
        rankdir=LR
        TerreTest [peripheries=2]
        Mars
        TerreTest -> Mars
    }
    ```

???- note "Collapsed Block with graphviz"

    ```graphviz dot cinq.svg
    digraph G {
        rankdir=LR
        TerreTest [peripheries=2]
        Mars
        TerreTest -> Mars
    }
    ```

```graphviz dot six.svg
digraph G {
    rankdir=LR
    TerreTest [peripheries=2]
    Mars
    TerreTest -> Mars
}
```


???+ note "Block with mermaid"

    ```mermaid
    graph TD
    A[Client] --> B[Load Balancer]
    B --> C[Server01]
    B --> D[Server02]
    B --> E[Server03]
    ```

```mermaid
graph TD
A[Client] --> B[Load Balancer]
B --> C[Server01]
B --> D[Server02]
B --> E[Server03]
```

## Mermaid

???- note "Collapsed Block with Mermaid"
    
    ```mermaid
    graph TD
    A[Client] --> B[Load Balancer]
    ```

    This is additional text.

???+ note "Open Block with Mermaid"

    ```mermaid
    graph TD
    A[Client] --> B[Load Balancer]
    B --> C[Server01]
    B --> D[Server02]
    B --> E[Server03]
    ```

```mermaid
graph TD
A[Client] --> B[Load Balancer]
B --> C[Server01]
B --> D[Server02]
B --> E[Server03]
```

~~~mermaid
%%{init: {'theme': 'dark' } }%%
graph TD
A --> B
~~~